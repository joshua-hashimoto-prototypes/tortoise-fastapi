import os
import re
from typing import Optional

from fastapi import FastAPI, APIRouter, Depends, Form, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jwt import decode as jwt_decode
from jwt import encode as jwt_encode
from passlib.hash import bcrypt

from .models import User, User_Pydantic
from app.resources.constants import EMAIL_REGEX, PASSWORD_REGEX

SECRET_KEY = os.environ.get('SECRET_KEY')

app_name = 'auth'

oauth2_schema = OAuth2PasswordBearer(tokenUrl=f'login')

router = APIRouter()


def setup_auth_routers(app: FastAPI):
    app.include_router(
        router,
        prefix=f'/{app_name}',
        tags=[app_name],
    )


async def authenticate_user(*, email: str, password: str) -> User:
    user = await User.get(email=email)
    if not user or not user.verify_password(password):
        return False
    return user


async def create_user(username: str, email: str, password: str) -> User_Pydantic:
    # validations
    if not email:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail='Email cannot be empty')
    if not password:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail='Password cannot be empty')
    if not re.match(EMAIL_REGEX, email):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail='Invalid email address')
    if not re.match(PASSWORD_REGEX, password):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail='Invalid password')
    # if username is not set make part of the email username
    if not username:
        username = email[:email.index('@')]
    # create user
    user_obj = User(
        username=username,
        email=email,
        password_hash=bcrypt.hash(password)
    )
    await user_obj.save()
    user = await User.get(email=email)
    user_pydantic = await User_Pydantic.from_tortoise_orm(user)
    return user_pydantic


async def get_current_user(token: str = Depends(oauth2_schema)) -> User_Pydantic:
    try:
        payload = jwt_decode(token, SECRET_KEY, algorithms=['HS256'])
        user = await User.get(id=payload.get('user_id'))
    except:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Invalid credentials')
    return await User_Pydantic.from_tortoise_orm(user)


def generate_jwt(user: User_Pydantic):
    token = jwt_encode({'user_id': str(user.id)},
                       SECRET_KEY, algorithm='HS256')
    return {'access_token': token, 'token_type': 'bearer'}


@router.post('/')
async def register_user(username: Optional[str] = Form(None), email: str = Form(...), password: str = Form(...)):
    """
    return access token if the registration is successful
    """
    user = await create_user(
        username=username,
        email=email,
        password=password,
    )
    return generate_jwt(user)


@router.post('/login')
async def login_user(form_data: OAuth2PasswordRequestForm = Depends()):
    """
    pass email address to form data 'username'
    """
    user = await authenticate_user(email=form_data.username, password=form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail='Invalid email or password')
    user_obj = await User_Pydantic.from_tortoise_orm(user)
    return generate_jwt(user_obj)


@router.get('/me', response_model=User_Pydantic)
async def current_user(user: User_Pydantic = Depends(get_current_user)):
    return user

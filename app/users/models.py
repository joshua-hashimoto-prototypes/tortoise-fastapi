from tortoise import Tortoise, fields
from tortoise.contrib.pydantic import pydantic_model_creator
from passlib.hash import bcrypt

from app.resources.core_model import CoreModel


class User(CoreModel):
    username = fields.CharField(50, unique=True)
    email = fields.CharField(60, unique=True)
    password_hash = fields.CharField(128)

    def __str__(self):
        return self.email

    def verify_password(self, password):
        return bcrypt.verify(password, self.password_hash)

    class PydanticMeta:
        exclude = ["password_hash"]


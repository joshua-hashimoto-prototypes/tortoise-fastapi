from fastapi import FastAPI

from .routers import setup_auth_routers


def setup_auth(app: FastAPI):
    setup_auth_routers(app)

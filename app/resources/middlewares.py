from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

# TODO: add production domain
origins = (
    'http:/localhost',
    'http://localhost:8080',
    'http://localhost:8000',
    'http://localhost:5000',
    'http://localhost:3000',
)


def setup_middleware(app: FastAPI):
    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=['*'],
        allow_headers=['*'],
    )

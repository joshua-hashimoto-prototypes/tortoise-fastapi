from fastapi import FastAPI
from tortoise import Tortoise
from tortoise.contrib.fastapi import register_tortoise


models_path_list = [
    'app.users.models',
    'app.contacts.models',
]


def get_db_uri(*, user, password, host, db):
    return f'postgres://{user}:{password}@{host}:5432/{db}'


def setup_database(app: FastAPI):
    register_tortoise(
        app,
        db_url=get_db_uri(
            user='postgres',
            password='postgres',
            host='db',  # docker-composeのservice名
            db='postgres',
        ),
        modules={
            'models': models_path_list,
        },
        generate_schemas=True,
        add_exception_handlers=True,
    )


Tortoise.init_models(models_path_list, 'models')

from fastapi import FastAPI

from .resources.middlewares import setup_middleware
from .resources.database import setup_database
from .users.main import setup_auth
from .contacts.main import setup_contacts

app = FastAPI(
    title='Tortoise-ORM + FastAPI Sample Application',
    version='1.0.0',
)


# configs
setup_middleware(app)
setup_database(app)

# setup "apps"
setup_auth(app)
setup_contacts(app)

# uvicorn app.main:app --reload

from fastapi import FastAPI

from .routers import setup_contacts_routers


def setup_contacts(app: FastAPI):
    setup_contacts_routers(app)

from tortoise.contrib.pydantic import pydantic_model_creator

from .models import Contact


Contact_Pydantic = pydantic_model_creator(Contact, name='Contact')
ContactIn_Pydantic = pydantic_model_creator(
    Contact, name='ContactIn', exclude_readonly=True)
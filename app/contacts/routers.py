from fastapi import FastAPI, APIRouter, Depends, Form, HTTPException, status

from .models import Contact, Contact_Pydantic
from app.users import get_current_user
from app.users.models import User_Pydantic


app_name = 'contacts'

router = APIRouter()


def setup_contacts_routers(app: FastAPI):
    app.include_router(
        router,
        prefix=f'/{app_name}',
        tags=[app_name],
    )


@router.post('/', status_code=status.HTTP_201_CREATED)
async def create_contact(contact_name: str = Form(...), user: User_Pydantic = Depends(get_current_user)):
    try:
        contact = Contact(user_id=user.id, name=contact_name)
        await contact.save()
        contact_obj = await Contact_Pydantic.from_tortoise_orm(contact)
        print(contact_obj.schema_json(indent=4))
    except Exception as err:
        print(err)
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail='failed to save data')
    return {'status_code': status.HTTP_201_CREATED, 'contact': contact_obj.dict()}


@router.get('/me')
async def get_all_contacts(user: User_Pydantic = Depends(get_current_user)):
    try:
        contacts = await Contact.filter(user_id=user.id)
        contacts_list = [await Contact_Pydantic.from_tortoise_orm(contact) for contact in contacts]
        print(user.schema_json())
    except:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail='failed to fetch related data')
    return {'contacts': contacts_list}

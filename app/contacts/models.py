from tortoise import fields
from tortoise.contrib.pydantic import pydantic_model_creator

from app.resources.core_model import CoreModel


class Contact(CoreModel):
    user = fields.ForeignKeyField(
        'models.User', related_name='contacts')
    name = fields.CharField(50)

    def __str__(self):
        return self.name


Contact_Pydantic = pydantic_model_creator(Contact, name='Contact')
ContactIn_Pydantic = pydantic_model_creator(
    Contact, name='ContactIn', exclude_readonly=True)
